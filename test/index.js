import request from 'request'
import chai from 'chai'
import mocks from './mocks'

import ethutils from 'ethereumjs-util'
import keys from 'keythereum'

const { debtor_request } = mocks

const expect = chai.expect
const url = 'https://localhost'
process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";

describe('Debt Capital Market API', () => {
    it('should respond', () => {
        request.get(url, {}, (error, response, body) => {
            expect(error).to.not.exist
            expect(response).to.exist
        })
    })

    it('send a debtor an order to sign then save it in the database', (done) => {
        return request.post({
            url: url + '/api/create',
            json: true,
            body: debtor_request,
        }, (error, response, body) => {
            expect(error).to.not.exist
            expect(response).to.exist
            expect(body.loanRequestHash).to.exist

            // sign order
            const signature = ethutils.ecsign(ethutils.toBuffer(body.loanRequestHash), mocks.pk)
            const signed_order = body
            signed_order.debtorSignature = {}
            signed_order.debtorSignature.v = ethutils.bufferToHex(signature.v)
            signed_order.debtorSignature.r = ethutils.bufferToHex(signature.r)
            signed_order.debtorSignature.s = ethutils.bufferToHex(signature.s)

            const derivedPubK = ethutils.ecrecover(ethutils.toBuffer(body.loanRequestHash), signature.v, signature.r, signature.s)
            const derivedAddress = '0x' + ethutils.pubToAddress(derivedPubK).toString('hex')

            expect(mocks.debtorAddress).to.equal(derivedAddress)
            return request.post({
                url: url + '/api/submit',
                json: true,
                body: signed_order,
            }, (error, response, body) => {
                expect(error).to.not.exist
                expect(body.result).to.be.true
                done()
            })
        })
    }).timeout(20000)

    it('should give creditors a list of orders then broadcast ones they sign', (done) => {
        return request.get({
            url: url + '/api/get'
        }, (error, response, body) => {
            expect(error).to.not.exist

            const orderToSign = JSON.parse(body)[0]

            expect(orderToSign).to.exist

            const signature = ethutils.ecsign(ethutils.toBuffer(orderToSign.loanRequestHash), mocks.pk)

            const signed_order = orderToSign
            signed_order.creditorSignature = {}
            signed_order.creditorSignature.v = ethutils.bufferToHex(signature.v)
            signed_order.creditorSignature.r = ethutils.bufferToHex(signature.r)
            signed_order.creditorSignature.s = ethutils.bufferToHex(signature.s)

            signed_order.creditorAddress = keys.privateKeyToAddress(mocks.pk)

            return request.post({
                url: url + '/api/fill',
                json: true,
                body: signed_order,
            }, (error, response, body) => {
                expect(error).to.not.exist
                expect(body.txHash).to.be.a('string')
                done()
            })
        })
    }).timeout(20000)

})


