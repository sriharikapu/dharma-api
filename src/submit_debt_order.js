import web3Utils from 'web3-utils'
import db from './db'
import validate from './validate'

const submitDebtOrder = (debtOrder) => {

    // TODO remove this
    db.saveDebtOrder(debtOrder)

    const wrongHash = {
        message: 'Loan Request Data Corrupted',
        result: false
    }

    const wrongSig = {
        message: `Signature didn't match your address`,
        result: false
    }

    const success = {
        message: 'Loan request sumbitted',
        result: true
    }

    // vallidate loanRequest 
    if (!validate.loanRequest(debtOrder)) return wrongHash

    if (!validate.signature(debtOrder.debtorSignature, debtOrder.loanRequestHash, debtOrder.issuance.debtor )) return wrongSig
    
    db.saveDebtOrder(debtOrder)
    return success
}

export default submitDebtOrder
