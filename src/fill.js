import fs from 'fs'
import EthereumTx from 'ethereumjs-tx'
import web3Utils from 'web3-utils'
import ethutil from 'ethereumjs-util'
import { dharma } from './connection'
import db from './db'
import { DebtOrderDataWrapper } from '@dharmaprotocol/dharma.js/dist/lib/src/wrappers/debt_order_data_wrapper.js'
import validate from './validate'

var debtKernel
const loadKernel = async () => {
    if (!debtKernel) {
        console.log(`FILL Loading Debt Kernel...`)
        debtKernel = await dharma.contracts.loadDebtKernelAsync()
        console.log(`FILL Debt Kernel Loaded!`)
    }
    return debtKernel
}
loadKernel()

const fillOrder = async (body) => {
    const {
        issuance,
        loanRequest,
        loanRequestHash,
        debtorSignature,
        creditorAddress,
        creditorSignature,
        terms,
    } = body

    console.log(`FILL Activated!`)

    // check for valid signatures
    if (!validate.loanRequest(body)) return error

    if (!validate.signature(creditorSignature, loanRequestHash, creditorAddress)) return error

    // Unpack debt order
    let debtOrderData = {
        creditor: creditorAddress,
        creditorFee: loanRequest.creditorFee,
        creditorSignature: creditorSignature,
        debtor: issuance.debtor,
        debtorFee: loanRequest.debtorFee,
        debtorSignature: debtorSignature,
        expirationTimestampInSec: loanRequest.expirationTimestampInSec,
        principalAmount: loanRequest.principalAmount,
        principalToken: loanRequest.principalToken,
        relayer: loanRequest.relayer,
        relayerFee: loanRequest.relayerFee,
        salt: issuance.salt,
        termsContract: issuance.termsContract,
        termsContractParameters: issuance.termsContractParameters,
        underwriter: issuance.underwriter,
        underwriterFee: loanRequest.underwriterFee,
        underwriterRiskRating: issuance.underwriterRiskRating,
        underwriterSignature: null,
    }

    const debtOrder = new DebtOrderDataWrapper(debtOrderData)
    const debtKernel = await loadKernel()

    const txData = {
        from: process.env.ETH_ADDRESS,
        to: debtKernel.address,
        nonce: await web3.eth.getTransactionCount(process.env.ETH_ADDRESS),
    }

    const tx = await debtKernel.applyDefaultsToTxDataAsync(
        txData,
        debtKernel.fillDebtOrder.estimateGasAsync.bind(
            debtKernel,
            debtOrder.getCreditor(),
            debtOrder.getOrderAddresses(),
            debtOrder.getOrderValues(),
            debtOrder.getOrderBytes32(),
            debtOrder.getSignaturesV(),
            debtOrder.getSignaturesR(),
            debtOrder.getSignaturesS(),
        )
    )

    tx.data = debtKernel.web3ContractInstance.fillDebtOrder.getData(
        debtOrder.getCreditor(),
        debtOrder.getOrderAddresses(),
        debtOrder.getOrderValues(),
        debtOrder.getOrderBytes32(),
        debtOrder.getSignaturesV(),
        debtOrder.getSignaturesR(),
        debtOrder.getSignaturesS(),
    )

    const TX = new EthereumTx(tx)
    TX.sign(Buffer.from(fs.readFileSync('/run/secrets/pk', 'utf8'), 'hex'))

    const serializedTx = '0x' + TX.serialize().toString('hex')

    web3.eth.sendSignedTransaction(serializedTx).on('transactionHash', hash => {
        console.log(`Transaction sent: ${hash}`);
    }).then(receipt => {
        console.log(`Transaction mined! tx ${receipt.transactionHash.substring(0,10)}.. mined on block ${receipt.blockNumber}`)
    }).catch(error => {
        console.error(`Error sending transaction: ${error}`)
    })

    // remove filled order from database
    db.removeDebtOrder(body)

    return '0x' + TX.hash().toString('hex')
}

export default fillOrder
