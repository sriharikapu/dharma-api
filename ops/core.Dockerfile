FROM alpine

RUN apk add --update nodejs bash curl
RUN npm install -g nodemon

COPY build/bundle.js /root/bundle.js

ENTRYPOINT ["node"]
CMD [ "/root/bundle.dev.js" ]
