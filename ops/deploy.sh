#!/usr/bin/env bash

########################################
# Development-specific environment variables

project=dcm
proxy_image="${project}_proxy"
core_image="${project}_core"

NODE_ENV="development"
DOMAINNAME="localhost"
EMAIL="noreply@example.com"

ETH_PROVIDER="https://kovan.infura.io/RNXFMnEXo6TEeIYzcTyQ"
ETH_ADDRESS="0x80e0d8d9917eb48cad307be74a4541cc93cd367a"
secret='dc8f8048926db106fa495484e1616b648ea027b1eb5174ede870f0072da7a011'

########################################

echo $secret | tr -d '\n\r' | docker secret create pk -

cat -> /tmp/compose.yml <<EOF
version: '3.4'

volumes:
  letsencrypt:
  devcerts:

secrets:
  pk:
    external: true

services:

  core:
    image: $core_image
    entrypoint:
      - nodemon
      - --exitcrash
      - --watch
      - /root/bundle.js
      - /root/bundle.js
    volumes:
      - type: bind
        source: `pwd`/build/bundle.js
        target: /root/bundle.js
    environment:
      - NODE_ENV=$NODE_ENV
      - ETH_PROVIDER=$ETH_PROVIDER
      - ETH_ADDRESS=$ETH_ADDRESS
    secrets:
      - pk
    ports:
      - "8000:8000"

  proxy:
    image: $proxy_image
    deploy:
      mode: global
    depends_on:
      - core
    volumes:
      - letsencrypt:/etc/letsencrypt
      - devcerts:/etc/letsencrypt/devcerts
    environment:
      - DOMAINNAME=$DOMAINNAME
      - EMAIL=$EMAIL
    ports:
      - "80:80"
      - "443:443"
EOF

docker stack deploy -c /tmp/compose.yml $project
rm /tmp/compose.yml

echo -n "Waiting for the api to wake up"
number_of_services=2
while true
do
    sleep 3
    if [[ "`docker container ls | grep $project | wc -l | sed 's/ //g'`" == "$number_of_services" ]]
    then
        echo " Good Morning!"
        sleep 10
        if [[ "`docker container ls | grep $project | wc -l | sed 's/ //g'`" == "$number_of_services" ]]
        then
            break
        fi
        echo -n "."
    else
        echo -n "."
    fi
done
