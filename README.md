# Motivation
- Crytpo-hedge funds of the future know debt & capital but they don't know or care about Dharma and Ethereum is mearly a means to an end. We're hosting a dharma relayer at ethindia.bohendo.com that keeps all dharma logic serverside so the client can focus on trading & can forget about dharma.js, the intricacies of web3.js, and paying gas.

# Problem we solved
- We're serving algorithmic trading bots & high frequency traders by hosting a Dharma relayer at ethindia.bohendo.com but this relayer sets itself apart from it's competition by hosting the dharma-specific logic serverside. This greatly streamlines trading bot development by providing a clean JSON API for interacting with Dharma. This opens up the potential markets that Dharma might serve by including those who don't have the resources necessary to manage a heavy client-side logic base.

# Challenges faced
- Our project involved using a library serverside which was built for the browser. Using code in an environment other than it was designed to operate is an invitation for chaos. We reimplemented chunks of the dharma.js logic & extracted internal components to, most challengingly, get transactions signed in a Node.js environment without Metamask available to help us.

# Client
 - See the example client library for interacting with this API at: https://gitlab.com/bohendo/dharma-client
